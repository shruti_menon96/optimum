from flask import Flask, render_template, json, request,jsonify
import os
import compileFile as cf
def write_to_file(code,input,ext):
    with open('main'+ext,'w') as file:
        file.write(code)
    file.close()
    with open('input.txt','w') as file:
        file.write(input)
    file.close()

app = Flask(__name__)

@app.route('/')
def main():
    return render_template('html/registration.html')

@app.route('/aboutuspage')
def aboutus():
    return render_template('html/newUserAboutUs.html')

@app.route('/helppage')
def help():
    return render_template('html/newUserHelp.html')

@app.route('/registerUser')
def help():
    return render_template('html/newUserHelp.html')


@app.route('/compileandrun',methods=['POST'])
def compileandrun(): 
    # read the posted values from the UI
    
    _code = request.form['code']
    _input = request.form['input']
    _language=request.form['language']
    # _code = request.form.get('code')
    # _input = request.form.get('input')
    # _language=request.form.get('language')
    _output=''
    if _code and _input and _language:
        # return json.dumps({'html':'All fields good !!'})
        if(_language=='c'):
            write_to_file(_code,_input,'.c')
        elif(_language=='cpp'):
            write_to_file(_code,_input,'.cpp')
        _output=cf.compiler('main.'+_language,'input.txt')
        return json.dumps({'code':_code,'input':_input,'output':_output,'language':_language})
    else:
        return json.dumps({'html':'<span>Enter the required fields</span>'+_code+'  '+_input+'  '+_language})

if __name__ == "__main__":
    app.run()
