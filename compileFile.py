import os

def readFile(filename):
    string=''
    with open(filename,'r') as file:
        string+=file.read()
    return string

def compiler(program_name,input_file):

    # program_name=str(sys.argv[1])
    # input_file=str(sys.argv[2])

    #checking for file name
    if(program_name.endswith('.c')):
        command_execute='gcc -o error '+program_name+' 2>error_gen.txt'
        os.system(command_execute)
        command_execute='gcc '+program_name
    elif(program_name.endswith('.cpp')):
        command_execute='g++ -o error '+program_name+' 2>error_gen.txt'
        os.system(command_execute)
        command_execute='g++ '+program_name
    else:
        command_execute=''
    os.system(command_execute)

    with open('error_gen.txt','r') as error_file_read:
        if(error_file_read.readline()==''):
            print('No error occured in codefile '+program_name)

            display_output_command='./a.out<'+input_file+' >output.txt'
            os.system(display_output_command)
            print('Please see your output in output.txt')
            return readFile('output.txt')
        else:
            #command for displaying the error
            print('Errror occurred')
            return readFile('error_gen.txt')
    error_file_read.close()


