$(function() {
    $('#btnSignUp').click(function() {
 
        $.ajax({
            url: '/signUp',
            data: $('form').serialize(),
            type: 'POST',
            success: function(response) {
                console.log(response);
                var resp=JSON.parse(response);
                $('#output').val(resp.output)
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});
