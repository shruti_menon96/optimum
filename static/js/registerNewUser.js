$(function() {
    $('#registerButton').click(function() { 
        var name=$('#firstname').val();
        var username=$('#username').val();
        var password=$('#pass1').val();
        var confirm_password=$('#pass2').val();
        var email=$('#email').val();
        // alert(code+input+language);
        $.ajax({
            url: '/registerUser',
            // data:"{'code':"+code+",'input':"+input+",'language':"+language+"}",
            data:{
                'name':name,
                'username':username,
                'pass1':password,
                'pass2':confirm_password,
                'email':email
            },
            type: 'POST',
            success: function(response) {
                console.log(response);
                var resp=JSON.parse(response);
                $('#output').val(resp.output)
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});
